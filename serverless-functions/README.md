# Serverless Functions

## Structure

- `./` contais all serverless functions used by the application.

### `./{function-name}`

- `handler.js` contains the function that will be deployed to the lambda ARN in AWS.
- `serverless.yml` contains the function configuration in the serverless framework.

## Deploy Function

- In the `./{function-name}` folder, after configure the `serverless.yml` run:

```bash
serverless deploy
```
