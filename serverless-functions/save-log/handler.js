'use strict';

const AWS = require('aws-sdk');

AWS.config.update({
  region: process.env.REGION
});

module.exports.saveLog = async (event) => {
  const logsDoc = new AWS.DynamoDB.DocumentClient({
    accessKeyId: process.env.ACCESS_KEY,
    secretAccessKey: process.env.ACCESS_SECRET,
    endpoint: process.env.DYNAMODB_ENDPOINT,
  });

  event.Records.forEach(record => {
    const { messageId, attributes: { ApproximateFirstReceiveTimestamp }, messageAttributes } = record;

    const params = {
      TableName: process.env.DYNAMODB_LOGS_TABLE,
      Item: {
        id: messageId,
        origin: messageAttributes.origin.stringValue,
        type: messageAttributes.type.stringValue,
        message: messageAttributes.message.stringValue,
        params: messageAttributes.params.stringValue,
        timestamp: ApproximateFirstReceiveTimestamp,
      },
    };

    logsDoc.put(params, (err, data) => {
      if (err) {
        console.error('Unable to add item. Error JSON:', JSON.stringify(err, null, 2));
      }

      console.log('Item added to database.');
    });
  });

  return;
};
