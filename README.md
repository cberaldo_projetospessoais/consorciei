# Log Service

## Project Structure

- `./log-endpoint` contains the application API used to call the lambda functions from AWS.
- `./serverless-functions` contains the application functions deployed at lambda ARN in AWS.

## Technologies

1. [Microservices with NodeJS](https://imasters.com.br/apis-microsservicos/api-gateway-em-arquitetura-de-microservices-com-node-js)

2. [Serverless](https://blog.rocketseat.com.br/serverless-nodejs-lambda/) structure used to execute `asyncronous` code that dont need immediate execution.

3. Styleguide [Airbnb](https://github.com/airbnb/javascript).

4. [NestJS](https://docs.nestjs.com/): scalable framework for server-side applications. Fully supports TypeScript and OOP/FP/FRP.
