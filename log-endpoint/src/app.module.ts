import { Module } from '@nestjs/common';
import { LogsController } from './logs/logs.controller';
import { LogsService } from './logs/logs.service';
import { AWSService } from './aws/aws.service';

@Module({
  imports: [],
  controllers: [LogsController], // enable log controller throught the application
  providers: [AWSService, LogsService], // enable log service throught the application
})
export class AppModule {}
