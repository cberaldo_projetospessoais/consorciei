import * as dotenv from 'dotenv';
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { json, urlencoded } from 'body-parser';
import { Logger, ValidationPipe } from '@nestjs/common';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const port = process.env.PORT || 3000;

  // enable json requests.
  app.use(json());
  // enable url encoded requests.
  app.use(urlencoded({ extended: true }));
  // enable cross-origin requests.
  app.enableCors();

  // enable request validation using data transfer objects.
  app.useGlobalPipes(new ValidationPipe());

  // start application.
  await app.listen(port, () => {
    Logger.log(`Application started, listening on http://localhost:${port}`, 'Consorciei API');
  });
}

dotenv.config();

bootstrap();
