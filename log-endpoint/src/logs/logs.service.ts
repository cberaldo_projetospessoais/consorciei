import { Injectable } from '@nestjs/common';
import { AttributeMap } from 'aws-sdk/clients/xray';
import { AWSService } from '../aws/aws.service';
import LogEntry from './dto/log-entry.dto';

@Injectable()
export class LogsService {
  tableName = 'consorciei-logs';

  constructor(private readonly awsService: AWSService) { }

  getLogsByOrigin(origin: string): Promise<AttributeMap[]> {
    const params = {
      TableName: this.tableName,
      ProjectionExpression: '#origin, #type, #message, #params, #timestamp',
      FilterExpression: '#origin = :origin',
      ExpressionAttributeNames: {
        '#type': 'type',
        '#origin': 'origin',
        '#message': 'message',
        '#params': 'params',
        '#timestamp': 'timestamp',
      },
      ExpressionAttributeValues: {
        ':origin': origin,
      },
      ScanIndexForward: false,
    };

    return this.awsService.scanDoc(params);
  }

  getLogsByType(type: string): Promise<AttributeMap[]> {
    const params = {
      TableName: this.tableName,
      ProjectionExpression: '#origin, #type, #message, #params, #timestamp',
      FilterExpression: '#type = :type',
      ExpressionAttributeNames: {
        '#type': 'type',
        '#origin': 'origin',
        '#message': 'message',
        '#params': 'params',
        '#timestamp': 'timestamp',
      },
      ExpressionAttributeValues: {
        ':type': type,
      },
      ScanIndexForward: false,
    };

    return this.awsService.scanDoc(params);
  }

  createLogEntry(logEntry: LogEntry) {
    const params = {
      DelaySeconds: 10,
      MessageAttributes: {
        origin: {
          StringValue: logEntry.origin,
          DataType: 'String',
        },
        type: {
          StringValue: logEntry.type,
          DataType: 'String',
        },
        message: {
          StringValue: logEntry.message,
          DataType: 'String',
        },
        params: {
          StringValue: JSON.stringify(logEntry.params),
          DataType: 'String',
        },
      },
      MessageBody: 'Sending log to SQS.',
      QueueUrl: process.env.SQS_ENDPOINT,
    };

    return this.awsService.sendSQSMessage(params);
  }
}
