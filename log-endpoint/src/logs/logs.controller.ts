import { Controller, Get, Query, Post, Body, Res } from '@nestjs/common';
import { LogsService } from './logs.service';
import LogEntry from './dto/log-entry.dto';
import LogResponse from './dto/log-response.dto';

@Controller()
export class LogsController {
  constructor(private readonly logService: LogsService) { }

  @Post('/create_log_entry')
  async create_log_entry(@Res() response, @Body() logEntry: LogEntry) {
    const result = new LogResponse();

    try {
      await this.logService
        .createLogEntry(logEntry);
    } catch (error) {
      result.setError(error.message);
    }

    result.sendResponse(response);
  }

  @Get('/get_logs_by_origin')
  async get_logs_by_origin(@Res() response, @Query('origin') origin: string) {
    const result = new LogResponse();

    try {
      result.body = await this.logService
        .getLogsByOrigin(origin);
    } catch (error) {
      result.setError(error.message);
    }

    result.sendResponse(response);
  }

  @Get('/get_logs_by_type')
  async get_logs_by_type(@Res() response, @Query('type') type: string) {
    const result = new LogResponse();

    try {
      result.body = await this.logService
        .getLogsByType(type);
    } catch (error) {
      result.setError(error.message);
    }

    result.sendResponse(response);
  }
}
