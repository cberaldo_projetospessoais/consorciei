import { IsNotEmpty, IsEnum } from 'class-validator';

enum LogType {
  log = 'log',
  warn = 'warn',
  error = 'error',
  custom = 'custom',
}

class LogEntry {
  @IsNotEmpty()
  origin: string;

  @IsNotEmpty()
  @IsEnum(LogType)
  type: string;

  @IsNotEmpty()
  message: string;

  params: object;
}

export default LogEntry;
