import { HttpStatus, Res } from '@nestjs/common';

class LogResponse {
  httpStatus: number;
  status: boolean;
  message: string;
  body: object;

  setError(message: string) {
    this.status = false;
    this.message = message;
    this.httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
  }

  getResponse() {
    return {
      status: this.status,
      message: this.message,
      body: this.body,
    };
  }

  sendResponse(@Res() response) {
    response
      .status(this.httpStatus)
      .send(this.getResponse());
  }

  constructor() {
    this.httpStatus = HttpStatus.OK;
    this.status = true;
  }
}

export default LogResponse;
