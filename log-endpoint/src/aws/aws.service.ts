import { Injectable } from '@nestjs/common';
import { DocumentClient } from 'aws-sdk/clients/dynamodb';
import { AttributeMap } from 'aws-sdk/clients/xray';
import * as AWS from 'aws-sdk';

@Injectable()
export class AWSService {
  docs: DocumentClient;

  scanDoc(params): Promise<AttributeMap[]> {
    // search document in AWS dynamoDB.
    return new Promise((resolve, reject) => {
      this.docs.scan(params, (err, data) => {
        if (err) {
          reject(err);
        } else {
          resolve(data.Items);
        }
      });
    });
  }

  sendSQSMessage(params) {
    const SQS = new AWS.SQS();
    // send message do AWS SQS.
    return new Promise((resolve, reject) => {
      SQS.sendMessage(params, (err, data) => {
        if (err) {
          reject(err.message);
        } else {
          resolve(data.MessageId);
        }
      });
    });
  }

  constructor() {
    // configure AWS sdk.
    AWS.config.update({
      region: process.env.REGION,
    });

    // load document client from DynamoDB.
    this.docs = new DocumentClient({
      accessKeyId: process.env.ACCESS_KEY,
      secretAccessKey: process.env.ACCESS_SECRET,
      endpoint: process.env.DYNAMODB_ENDPOINT,
    });
  }
}
