# Log Endpoint

## Project Structure

- `src`
- `src/main.ts`
- `src/app.module.ts`
- `src/{module-name}/{module-name}.controller.ts`
- `src/{module-name}/{module-name}.service.ts`
- `src/{module-name}/dto/{request-name}.dto.ts`

### `main.ts`

Entry point for the application.

### `src/app.module.ts`

Main module that loads all services, providers and another modules used and enable then throught the application.

### `{module-name}.controller.ts`

Enabled methods and routes for the module.

### `{module-name}.service.ts`

Functions and services used by the application controller.

### `dto/{request-name}.dto.ts`

Requests models used to validate the received information.

More information at [NestJS](https://docs.nestjs.com) docs.

## Development

To start project in development mode:

1. Configure `.env` file.

2. Install application dependencies:

```bash
npm install
```

3. Start the application in development mode:

```bash
npm run start:dev
```

4. Start the application in production mode:

```bash
npm run build

npm run start:prod
```

## Testing the Application

- Get logs by origin:

```curl
curl -X GET \
  'http://localhost:3000/get_logs_by_origin/?origin=login' \
    -H 'Cache-Control: no-cache' \
```

- Get logs by type:

```curl
curl -X GET \
  'http://localhost:3000/get_logs_by_type/?type=log' \
  -H 'Cache-Control: no-cache' \
```

- Create log entry:

```curl
curl -X POST \
  http://localhost:3000/create_log_entry \
    -H 'Cache-Control: no-cache' \
    -H 'Content-Type: application/json' \
    -d '{
      "origin": "login",
      "type": "warn",
      "message": "Request Login failed for some reason",
      "params": { "userId": 6 }
    }'
```
